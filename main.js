let color = "black";
let click = false;

function populateBoard(size) {
  // select the board
  let board = document.querySelector(".board");

  //clear any existing squares on the board
  let squares = board.querySelectorAll("div");
  squares.forEach((div) => div.remove());

  // set the css size of the grid to the inserted value
  board.style.gridTemplateColumns = `repeat(${size}, 1fr)`;
  board.style.gridTemplateRows = `repeat(${size}, 1fr)`;

  // create the individual boxes in the CSS grid
  for (let i = 0; i < size * size; i++) {
    let square = document.createElement("div");
    square.addEventListener("mouseover", colorSquare);
    square.style.backgroundColor = "white";
    board.insertAdjacentElement("beforeend", square);
  }
}

// initialize the grid to size 16
populateBoard(16);

// change the size of the grid
function changeSize(input) {
  if (input >= 2 || input <= 100) populateBoard(input);
  else console.log("invalid size");
}

function colorSquare() {
  if (click) {
    if (color === "random") {
      this.style.backgroundColor =
        "#" + (((1 << 24) * Math.random()) | 0).toString(16);
    } else this.style.backgroundColor = color;
  }
}

function changeColor(choice) {
  color = choice;
}

function resetBoard() {
  let board = document.querySelector(".board");
  let squares = board.querySelectorAll("div");
  squares.forEach((div) => (div.style.backgroundColor = "white"));
}

// when mouse button is clicked start chaning color
document
  .querySelector("body")
  .addEventListener("mousedown", () => (click = true));
// when mouse button is not cliked do nothing
document
  .querySelector("body")
  .addEventListener("mouseup", () => (click = false));
